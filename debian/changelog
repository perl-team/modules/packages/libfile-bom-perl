libfile-bom-perl (0.18-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libfile-bom-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 01:15:19 +0100

libfile-bom-perl (0.18-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/*: replace ADTTMP with AUTOPKGTEST_TMP.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.4.1, no changes needed.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.
  * Import upstream version 0.18.
  * Add debian/upstream/metadata.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 May 2020 22:30:18 +0200

libfile-bom-perl (0.16-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.16.
  * Add /me to Uploaders.
  * Declare compliance with Debian Policy 4.3.0.
  * Bump debhelper compatibility level to 11.

 -- gregor herrmann <gregoa@debian.org>  Sat, 09 Feb 2019 02:19:56 +0100

libfile-bom-perl (0.15-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop spelling.patch, merged upstream.

 -- gregor herrmann <gregoa@debian.org>  Tue, 15 Dec 2015 18:33:57 +0100

libfile-bom-perl (0.14-3) unstable; urgency=medium

  * Team upload.
  * Add a patch to fix autopkgtest. Thanks to ci.debian.net
  * Add a patch to fix a spelling mistake in the POD.
  * debian/copyright: update license stanzas.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Wed, 12 Aug 2015 20:56:09 +0200

libfile-bom-perl (0.14-2) unstable; urgency=medium

  * Team upload.

  [ Ryan Niebur ]
  * Update jawnsy's email address

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.
  * Remove alternative (build) dependencies that are already satisfied
    in oldstable.
  * debian/control: update {versioned,alternative} (build) dependencies.
  * Strip trailing slash from metacpan URLs.

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"
  * Mark package as autopkgtestable.
  * Declare compliance with Debian Policy 3.9.6.
  * Switch to source format "3.0 (quilt)".
  * Add explicit build dependency on libmodule-build-perl.
  * Fix lintian warning copyright-refers-to-symlink-license.
  * Reindent debian/copyright.

 -- Axel Beckert <abe@debian.org>  Sat, 06 Jun 2015 23:47:41 +0200

libfile-bom-perl (0.14-1) unstable; urgency=low

  * Initial Release (Closes: #536389)

 -- Jonathan Yu <frequency@cpan.org>  Thu, 9 Jul 2009 08:22:36 -0400
